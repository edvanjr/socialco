package com.socialco.api.dto;

public class UserDTO {
	private String personid;
	private String name;
	
	public UserDTO(String personid, String name) {
		this.personid = personid;
		this.name = name;
	}
	
	public String getPersonid() {
		return personid;
	}
	public void setPersonid(String personid) {
		this.personid = personid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
