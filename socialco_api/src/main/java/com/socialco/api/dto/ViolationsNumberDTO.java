package com.socialco.api.dto;

public class ViolationsNumberDTO {
	private String violation;
	private int number;
	
	public ViolationsNumberDTO(String violation, int number) {
		this.violation = violation;
		this.number = number;
	}
	public String getViolation() {
		return violation;
	}
	public void setViolation(String violation) {
		this.violation = violation;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
	
}
