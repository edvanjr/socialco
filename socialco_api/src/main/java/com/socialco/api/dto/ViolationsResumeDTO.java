package com.socialco.api.dto;

public class ViolationsResumeDTO {

	private String personid;
	private String user;
	private Integer quantityDataRange;
	private Integer quantityDailySessionsLimit;
	private Integer quantityInactiveUsers;
	private Integer quantityMaxCharacters;
	private Integer quantityMaximumActiveSession;
	

	public ViolationsResumeDTO() {
		this.quantityDataRange = 0;
		this.quantityDailySessionsLimit = 0;
		this.quantityInactiveUsers = 0;
		this.quantityMaxCharacters = 0;
		this.quantityMaximumActiveSession = 0;

	}

	public String getPersonid() {
		return personid;
	}

	public void setPersonid(String personid) {
		this.personid = personid;
	}

	public Integer getQuantityDataRange() {
		return quantityDataRange;
	}

	public void setQuantityDataRange(Integer quantityDataRange) {
		this.quantityDataRange = quantityDataRange;
	}

	public Integer getquantityDailySessionsLimit() {
		return quantityDailySessionsLimit;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setquantityDailySessionsLimit(Integer quantityDailySessionsLimit) {
		this.quantityDailySessionsLimit = quantityDailySessionsLimit;
	}

	public Integer getQuantityInactiveUsers() {
		return quantityInactiveUsers;
	}

	public void setQuantityInactiveUsers(Integer quantityInactiveUsers) {
		this.quantityInactiveUsers = quantityInactiveUsers;
	}

	public Integer getQuantityMaxCharacters() {
		return quantityMaxCharacters;
	}

	public void setQuantityMaxCharacters(Integer quantityMaxCharacters) {
		this.quantityMaxCharacters = quantityMaxCharacters;
	}

	public Integer getQuantityMaximumActiveSession() {
		return quantityMaximumActiveSession;
	}

	public void setQuantityMaximumActiveSession(Integer quantityMaximumActiveSession) {
		this.quantityMaximumActiveSession = quantityMaximumActiveSession;
	}

}
