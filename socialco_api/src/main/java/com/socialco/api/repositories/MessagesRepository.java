package com.socialco.api.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.socialco.api.models.Messages;

@Repository
public interface MessagesRepository extends JpaRepository<Messages,Integer> {

}
