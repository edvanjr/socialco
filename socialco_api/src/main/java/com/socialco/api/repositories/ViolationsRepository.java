package com.socialco.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialco.api.models.Violations;

@Repository
public interface ViolationsRepository extends JpaRepository<Violations, Integer> {

	@Query("SELECT DISTINCT v.personid  FROM Violations v")
	List<String> listPersonIds();

	@Query("SELECT DISTINCT v.googleid  FROM Violations v WHERE v.personid = :personid AND v.hangoutid != ''")
	List<String> listGoogleIds(@Param("personid") String personid);

	@Query("FROM Violations v WHERE v.googleid = :googleid")
	List<Violations> listViolations(@Param("googleid") String googleid);
	
	@Query("SELECT DISTINCT v.type FROM Violations v")
	List<String> listViolationsTypes();
	
	@Query("SELECT COUNT(*) FROM Violations v WHERE v.type = :type AND v.personid = :personid ")
	Integer countViolationsByUser(@Param("type") String type, @Param("personid") String personid);
	
	@Query("SELECT COUNT(*) FROM Violations v WHERE v.type = :type")
	Integer countViolations(@Param("type") String type);

}
