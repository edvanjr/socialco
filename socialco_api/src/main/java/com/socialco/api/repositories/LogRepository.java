package com.socialco.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.socialco.api.dto.UserDTO;
import com.socialco.api.models.Log;

@Repository
public interface LogRepository extends JpaRepository<Log,Integer> {
	@Query("SELECT DISTINCT l.googleid  FROM Log l WHERE l.personid = :personid AND l.hangoutid != ''")
	List<String> listHangoutIds(@Param("personid")String personid);
	
	@Query("SELECT DISTINCT l.personid  FROM Log l")
	List<String> listPersonIds();
	
	@Query("FROM Log l WHERE l.googleid = :googleid")
	List<Log> listLogs(@Param("googleid")String googleid);
	
	@Query("SELECT DISTINCT new com.socialco.api.dto.UserDTO(l.personid, l.user) FROM Log l ORDER BY l.user")
	List<UserDTO> listUsers();
}
