package com.socialco.api.models;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="violations")
public class Violations {
	
	
	@Id
	private Integer violationid;
	private String hangoutid;;
	private String googleid;
	private String personid;
	private Date date;
	private String type;
	
	public Integer getViolationid() {
		return violationid;
	}
	public void setViolationid(Integer violationid) {
		this.violationid = violationid;
	}
	public String getHangoutid() {
		return hangoutid;
	}
	public void setHangoutid(String hangoutid) {
		this.hangoutid = hangoutid;
	}
	public String getGoogleid() {
		return googleid;
	}
	public void setGoogleid(String googleid) {
		this.googleid = googleid;
	}
	public String getPersonid() {
		return personid;
	}
	public void setPersonid(String personid) {
		this.personid = personid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
