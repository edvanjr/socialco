package com.socialco.api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.socialco.api.dto.ViolationsNumberDTO;
import com.socialco.api.repositories.ViolationsRepository;

@RestController
@RequestMapping("/violations")
public class ViolationsController {

	@Autowired
	private ViolationsRepository violationsRepository;

	@ResponseBody
	@RequestMapping(value="/violations-user", method = RequestMethod.POST)
	public List<ViolationsNumberDTO> getViolationsByUser(@RequestBody String personid){
		List<String> violationsTypes = violationsRepository.listViolationsTypes();
		List<ViolationsNumberDTO> violations = new ArrayList<>();
		
		for(String v: violationsTypes){
			int countViolation = violationsRepository.countViolationsByUser(v, personid);
			ViolationsNumberDTO violation = new ViolationsNumberDTO(v, countViolation);
			violations.add(violation);
		}
		
		return violations;
	}
	
	@ResponseBody
	@RequestMapping(value="/violations-total")
	public List<ViolationsNumberDTO> getViolations(){
		List<String> violationsTypes = violationsRepository.listViolationsTypes();
		List<ViolationsNumberDTO> violations = new ArrayList<>();
		
		for(String v: violationsTypes){
			int countViolation = violationsRepository.countViolations(v);
			ViolationsNumberDTO violation = new ViolationsNumberDTO(v, countViolation);
			violations.add(violation);
		}
		
		return violations;
	}

}