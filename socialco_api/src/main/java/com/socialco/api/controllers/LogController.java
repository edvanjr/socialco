package com.socialco.api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.socialco.api.dto.LogResumeDTO;
import com.socialco.api.dto.UserDTO;
import com.socialco.api.models.Log;
import com.socialco.api.repositories.LogRepository;

@RestController
@RequestMapping("/log")
public class LogController {

	@Autowired
	private LogRepository logRepository;
	
	@RequestMapping(value="/list-resume")
	public List<LogResumeDTO> listLogsResume() {
		
		List<LogResumeDTO> logs = new ArrayList<>();
		List<String> personIds = logRepository.listPersonIds();
		
		for (String p: personIds){
			List<String> hangoutIds = logRepository.listHangoutIds(p);
			
			for(String s: hangoutIds) {
				List<Log> foundLogs = logRepository.listLogs(s);
				if(foundLogs.size() == 2) {
					if(!foundLogs.get(0).getAction().equalsIgnoreCase(foundLogs.get(1).getAction())) {
						
						long difference = foundLogs.get(1).getDate().getTime() - foundLogs.get(0).getDate().getTime();
						long differenceMinutes = difference / (60 * 1000);
						
						LogResumeDTO resume = new LogResumeDTO();
						resume.setGoogleid(foundLogs.get(0).getGoogleid());
						resume.setDate(foundLogs.get(0).getDate());
						resume.setUser(foundLogs.get(0).getUser());
						resume.setTimeSpent(Math.abs(differenceMinutes));
						
						logs.add(resume);
					}
				} else if (foundLogs.size() > 2){
					System.out.println(foundLogs.get(0).getGoogleid());
				}
			}
		}
		
		return logs;
	}
	
	@RequestMapping(value="/users")
	public List<UserDTO> listUsers() {
		return logRepository.listUsers();
	}
}
