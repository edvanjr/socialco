package com.socialco.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.socialco.api.models.Messages;
import com.socialco.api.repositories.MessagesRepository;

@RestController
@RequestMapping("/messages")
public class MessagesController {
	
	@Autowired
	private MessagesRepository messagesRepository;
	
	@RequestMapping(value="/list")
	public List<Messages> list() {
		return messagesRepository.findAll();
	}
}
