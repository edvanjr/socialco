app.factory('socialAPI', function($http){
	var url = 'http://54.215.191.231:32343/socialco_api/';

	var _getUsers = function() {
		return $http.get(url + 'log/users');
	}

	var _getViolationsByUser = function(userid) {
		return $http.post(url + 'violations/violations-user', userid);
	}

	var _getTotalViolations = function() {
		return $http.get(url + 'violations/violations-total');
	}

	return {
		getUsers: function() {
			return _getUsers();
		},
		getViolationsByUser: function(userid) {
			return _getViolationsByUser(userid);
		},
		getTotalViolations: function() {
			return _getTotalViolations();
		}

	};
});