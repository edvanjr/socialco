app.config(function($routeProvider, $locationProvider){
	$routeProvider
   .when('/', {
      templateUrl : 'views/time-spent.html',
      controller     : 'TimeSpentCtrl'
   })
   .when('/time-spent', {
      templateUrl : 'views/time-spent.html',
      controller     : 'TimeSpentCtrl'
   })
   .when('/violations', {
      templateUrl : 'views/violations.html',
      controller     : 'ViolationsCtrl'
   })
   .otherwise ({ redirectTo: '/' });
});