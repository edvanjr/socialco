app.controller('ViolationsCtrl', function($rootScope, $scope, $http, $location, socialAPI){
	 
   $scope.users = [];
   $scope.userSelected = "";
   $scope.numberOfViolationsByUser = [];
   $scope.violationsByUser = [];
   $scope.totalViolationsNumber = [];
   $scope.totalViolations = [];

   ($scope.getUsers = function() {
	    socialAPI.getUsers().success(function(data) {
	      $scope.users = data;
	    });
   })();

   ($scope.getTotalViolations = function() {
	    socialAPI.getTotalViolations().success(function(data) {
	      	$scope.totalViolationsNumber = data.map(x=>x['number']);
			$scope.totalViolations = data.map(x=>x['violation']);
	    });
   })();

   $scope.getViolationsByUser = function(){
   		socialAPI.getViolationsByUser($scope.userSelected).success(function(data) {
   			$scope.numberOfViolationsByUser = data.map(x=>x['number']);
			$scope.violationsByUser = data.map(x=>x['violation']);
	    });
   }
           
});
