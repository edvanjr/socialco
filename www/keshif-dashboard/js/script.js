kshf.lang.cur = kshf.lang.en;

function loadData(browser){
  $.ajax( {
    url: "http://54.215.191.231:32343/socialco_api/log/list-resume",
    dataType: "json",
    success: function(data){
        browser.primaryTableName = "Logs";
        kshf.dt.Logs = [];
        data.forEach(function(p){
          
          var log = {}

          
            log.User = p.user;
            log.Date = new Date(p.date);
            log["Time Spent (in minutes)"] =  p.timeSpent;
           
           
           
            kshf.dt.Logs.push(new kshf.Record(log,'id'));
        });
       
        browser.loadCharts();
    }
  });
};
 
$(document).ready( function(){
      browser = new kshf.Browser({
        domID: "#demo_Browser",
        itemName: "Logs Resume",
        categoryTextWidth: 250,
        source: { callback: loadData },
        summaries: [
            { name: "User", panel: "middle"},
            { name: "Time Spent (in minutes)", panel: "middle" },          
            { name: "Date", panel: "middle"}
        ]
      });
});